
using AutoMapper;
using Dapper;
using Dapper.Oracle;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconBronzeOrders.Models;
using RoconLibrary.Utility;
using RoconLibrary;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RoconBronzeOrders.Utility
{
    class SQLHelper
    {
        public static async Task<Order> GetOrderByIdAsync(OrderStatusEntity orderStatusEntity,
                                                     IDbConnection connection, 
                                                     IMapper mapper)
        {
            var orderEntity = await getOrderEntity(orderStatusEntity, connection);
            return mapper.Map<Order>(orderEntity);
        }

        private static async Task<OrderEntity> getOrderEntity(OrderStatusEntity orderStatusEntity, 
                                                              IDbConnection connection)
        {
            string orderID = orderStatusEntity.OrderNumber;
            Log.Debug($"=====> Order Id is: {orderID}");
            Log.Debug($"=====> Status is: {orderStatusEntity.Status}");

            var sQuery = getOrderQuery(orderID);

            Log.Information($"\n\n {sQuery} \n\n");

            var results = await connection.QueryAsync<OrderHeaderEntity, RouteInfoEntity, OrderShipToInfoEntity,
                        OrderCommentsInfoEntity, PackingInvoiceCommentEntity, InvoiceInfoEntity, DeliveryChargeEntity,
                        OrderHeaderEntity>(sQuery, map: (header, route, ship, comment, packing, inv, delivery) =>
                        {
                            header.routeInfo = route;
                            header.shipToInfo = ship;
                             
                            if (comment != null)
                            {
                                header.orderCommentsInfo = comment;
                            }

                            if (packing != null && header.orderCommentsInfo != null)
                            {
                                header.orderCommentsInfo.PackingInvoiceComment = packing;
                            }

                            header.invoiceInfo = inv;
                            header.invoiceInfo.DeliveryCharge = delivery;
                            return header;
                        }
                            , splitOn: "routeCode,name,bolcomment,textLine1,invoiceNumber,deliveryChargeType"
                            );

            var orderEntity = new OrderEntity();

            var orderHeaderInfo = results.First();

            foreach(OrderHeaderEntity orderHeaderEntity in results)
            {
                if(orderHeaderEntity.invoiceInfo.InvoiceType.Equals("A"))
                {
                    orderHeaderInfo = orderHeaderEntity;
                    break;
                }
            }

            orderEntity.orderHeader = orderHeaderInfo;

            setStatusDetail(orderEntity, orderStatusEntity, connection);
            var orderItems = await GetOrderItemDetails(orderStatusEntity, connection);
            orderEntity.orderItems = orderItems.ToArray();

            if (orderEntity.orderHeader == null)
            {
                throw new Exception($"The order header was null for order id [{orderID}]] for query [{sQuery}]");
            }
            
            return orderEntity;
        }

        private static string getOrderQuery(string sorOrderNumber) {
            return "SELECT DISTINCT HDR.ORDER_TYPE as orderType"
                    + " ,TRIM(HDR.CU_NO) as customerNumber"
                    + " ,TRIM(CUS.CU_TYPE) as customerType"
                    + " ,LCO.lcoOrderNumber as lcoOrderNumber"
                    + " ,LCO.sorORderNumber as sorOrderNumber"
                    + " ,'STATUS_UPDATE' as orderEvent"
                    + " ,LCO.orderRequestedDate as orderRequestDate"
                    + " ,HDR.ORDER_DT_TM as orderCreationDate"
                    + " ,HDR.ORDER_REQD_DT as orderRequiredDate"
                    + " ,HDR.SHIP_REQD_DT as orderShipmentDate"
                    + " ,LCO.DeliveryType deliveryType"
                    + " ,'Y' trafficPlanningIndicator"
                    + " ,'N' uncommittedItemQtyIndicator"
                    + " ,HDR.PAY_METHOD airMilesNo"
                    + " ,HDR.PRIOR_NAME orderPriority"
                    + " ,HDR.HOST_ODNO hostOrderNumber"
                    + " ,HDR.TRAFFIC_REF_NO referenceNumber"
                    + " ,HDR.CARRIER carrierId"
                    + " ,HDR.SERVICE_LEVEL serviceLevel"
                    + " ,TTL.ITEM_COUNT numberOfLineItems"
                    + " ,HDR.TOTAL_CASES numberOfFullCases"
                    + " ,HDR.ROUTE_CODE as routeCode"
                    + " ,HDR.ROUTE_STOP as routeStop"
                    + " ,DECODE(LCO.OverwriteShipToInformation, 0, CUS.CU_NAME, LCO.OverWriteShipToName) AS name"
                    + " ,DECODE(LCO.OverwriteShipToInformation, 0, CUS.ADD1_SH, LCO.OverWriteShipToAddressLine1) AS addressLine1"
                    + " ,DECODE(LCO.OverwriteShipToInformation, 0, CUS.ADD2_SH, LCO.OverWriteShipToAddressLine2) AS addressLine2"
                    + " ,ltrim(rtrim(DECODE(LCO.OverwriteShipToInformation, 0, CUS.CITY_SH, LCO.OverWriteShipToCity))) AS city"
                    + " ,DECODE(LCO.OverwriteShipToInformation, 0, CUS.PROV_SH, LCO.OVERWRITESHIPTOPROVINCECODE) AS provinceCode"
                    + " ,DECODE(LCO.OverwriteShipToInformation, 0, CUS.COUNTRY, LCO.OVERWRITESHIPTOCOUNTRY) AS country"
                    + " ,DECODE(LCO.OverwriteShipToInformation, 0, CUS.POST_CODE, LCO.OverWriteShipToPostalCode) AS postalCode"
                    + " ,REGEXP_REPLACE(DECODE(LCO.OverwriteShipToInformation, 0, CUS.PHONE_NU, LCO.OVERWRITESHIPTOPHONENUMBER),'[^0-9]','') AS phoneNumber"
                    + " ,LCO.OverwriteShipToInformation overwriteShipToInformation"
                    + " ,LCBO.GET_ORDER_COMMENTS(HDR.CO_ODNO,'B',1) bolComment"
                    + " ,LCBO.GET_ORDER_COMMENTS(HDR.CO_ODNO,'P',1) pickerComment"
                    + " ,LCBO.GET_ORDER_COMMENTS(HDR.CO_ODNO,'S',1) shipLabelComment"
                    + " ,LCBO.GET_ORDER_COMMENTS(HDR.CO_ODNO,'L',1) textLine1"
                    + " ,LCBO.GET_ORDER_COMMENTS(HDR.CO_ODNO,'L',2) textLine2"
                    + " ,LCBO.GET_ORDER_COMMENTS(HDR.CO_ODNO,'L',3) textLine3"
                    + " ,trim(TTL.CO_ODNO) invoiceNumber"
                    + " ,TTL.CO_INV_TYPE invoiceType"
                    + " ,TTL.INV_TOTAL invoiceTotalAmount"
                    + " ,TTL.DISC_TOTAL invoiceDiscountAmount"
                    + " ,TTL.BOTTLE_DEPOSIT_TOTAL invoiceBottleDepositAmount"
                    + " ,TTL.TAX_TOTAL invoiceHSTAmount"
                    + " ,TTL.LICMU_TOTAL invoiceLicMUAmount"
                    + " ,TTL.LEVY_TOTAL invoiceLevyAmount"
                    + " ,DECODE(LCO.DeliveryType,'SHIP_TO_ADDR_ON_FILE','CALCULATE DELIVERY','PICKUP') as deliveryChargeType"
                    + " ,0 as deliveryBaseAmount"
                    + " ,0 as deliveryHSTAmount"
                    // + " ,TTL.RETAIL_TOTAL as invoiceRetailAmount"
                    + " FROM LCBO.V_ORDERS HDR INNER JOIN DBO.ON_CUST_MAS CUS ON CUS.CU_NO = HDR.CU_NO"
                    + $" INNER JOIN LCBO.V_LCO_ORDER_TOTALS TTL ON TTL.CO_ODNO = rpad('{sorOrderNumber}',16)"
                    + $" INNER JOIN LCBO.LCO_ORDERS LCO ON rpad(LCO.SORORDERNUMBER, 16) = rpad('{sorOrderNumber}',16)"
                    + $" WHERE HDR.CO_ODNO = rpad('{sorOrderNumber}',16)";
        }

        private static void setStatusDetail(OrderEntity orderEntity, 
                                        OrderStatusEntity orderStatusEntity, 
                                        IDbConnection connection) 
        {
            var sql = "SELECT CO_ODNO, CO_STATUS AS STATUS, \"TIMESTAMP\" AS STATUS_DT_TM" 
                            + " FROM DBO.COHDR"
                            + " WHERE ENTRY_FL = 'O' AND order_type = 'AGY'"
                            + " AND CO_ODNO = rpad(:ORDER_NO, 16)"
                        + " UNION"                          
                            + " SELECT CO_ODNO, CO_STATUS AS STATUS, \"TIMESTAMP\" AS STATUS_DT_TM" 
                            + " FROM DBO.ON_COHDR"
                            + " WHERE ENTRY_FL = 'O' AND order_type = 'AGY'"
                            + " AND CO_ODNO = rpad(:ORDER_NO, 16)";

            // Log.Information($"*** Status sql: {sql}");
            var command = new OracleCommand(sql, (OracleConnection) connection);
            command.Parameters.Add(new OracleParameter("ORDER_NO", orderStatusEntity.OrderNumber));
            var oraclReader = command.ExecuteReader();

            while (oraclReader.Read())
            {
                Log.Information("**** Setting order status detail ....");
                orderEntity.orderHeader.orderStatus = new OrderStatusCodeDetailEntity();
                orderEntity.orderHeader.orderStatus.orderStatusCode = ((string) oraclReader["STATUS"]).Trim();
                if(orderEntity.orderHeader.orderStatus.orderStatusCode.Equals(OrderStatus.C.ToString()))
                {
                    orderEntity.orderHeader.orderStatus.orderStatusName = "C:CLOSED";
                    orderStatusEntity.Status = OrderStatus.C.ToString();
                }
                else if(orderEntity.orderHeader.orderStatus.orderStatusCode.Equals(OrderStatus.D.ToString()))
                {
                    orderEntity.orderHeader.orderStatus.orderStatusName = "D:CANCELED";
                    orderStatusEntity.Status = OrderStatus.D.ToString();
                }
                else if(orderEntity.orderHeader.orderStatus.orderStatusCode.Equals(OrderStatus.O.ToString()))
                {
                    orderEntity.orderHeader.orderStatus.orderStatusName = "O:OPENED";
                    orderStatusEntity.Status = OrderStatus.O.ToString();
                }
                else 
                {
                    throw new Exception($"Unknown status code: {orderEntity.orderHeader.orderStatus.orderStatusCode} for order id: {orderStatusEntity.OrderNumber}");
                }
                orderEntity.orderHeader.orderStatus.statusCreateTime = FormatUtils.AddTimezone(((DateTime)oraclReader["STATUS_DT_TM"]).ToString());
            }
            oraclReader.Close();
        }

        private static async Task<IEnumerable<OrderItemsEntity>> GetOrderItemDetails(OrderStatusEntity orderStatusEntity, 
                                                                        IDbConnection connection)
        {
            string RocOrderID = orderStatusEntity.OrderNumber;
            string OrderInvType = getOrderEventType(orderStatusEntity);

            var sQuery = "LCBO.GET_ORDER_DETAIL_BY_STATUS_V22";

            var param = new OracleDynamicParameters();
            param.Add("lv_sorOrderNumber", RocOrderID);
            param.Add("lv_lco_status", orderStatusEntity.Status);
            param.Add("lv_orderDetails", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

            var results = await connection.QueryAsync<OrderItemsEntity, OrderItemPriceInfoEntity, OrderItemsEntity>(sQuery, map: (item, price) =>
            {
                item.priceinfo = price;
                return item;
            }, splitOn: "sellingPrice", param: param, commandType: CommandType.StoredProcedure);

            return results.ToArray();
        }
        
        private static string getOrderEventType(OrderStatusEntity orderStatusEntity) {
            var orderEventType = "E";
            if(string.Equals(orderStatusEntity.Status, OrderStatus.I.ToString()) 
                     || string.Equals(orderStatusEntity.Status, OrderStatus.C.ToString())) {
                orderEventType = "A";
            }
            return orderEventType;
        }
    }
}

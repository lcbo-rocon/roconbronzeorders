using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RoconBronzeOrders.Services;
using RoconBronzeOrders.Services.Implementation;
using RoconLibrary.Utility;
using System;
using System.IO;

public class Startup
{
    IConfigurationRoot Configuration { get; }

    public Startup()
    {
        string environmentKey = "ASPNETCORE_ENVIRONMENT";
        string environment = Environment.GetEnvironmentVariable(environmentKey);
        Console.WriteLine($"Current directory is: {Directory.GetCurrentDirectory()}");

        string appSettings = null;

        if (environment != null)
        {
            appSettings = $"appsettings.{environment}.json";
        }
        else
        {
            string errorMessage = $"The environment variable [{environmentKey}] was not found.\n";
            errorMessage = errorMessage + $"Correct values are: Development, Staging or Production";
            throw new Exception(errorMessage);
        }

        Console.WriteLine($"Using appsetting file: {appSettings}");

        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile(appSettings);

        Configuration = builder.Build();
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<IConfigurationRoot>(Configuration);
        services.AddSingleton<IBronzeOrderService, BronzeOrderService>();

        var mappingConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MapperProfile());
        });

        IMapper mapper = mappingConfig.CreateMapper();
        services.AddSingleton(mapper);
    }
}
﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Utility;
using RoconBronzeOrders.Utility;
using RoconBronzeOrders.Models;
using System.Data;
using System.Threading.Tasks;
using RoconLibrary.Domains;
using AutoMapper;
using Serilog;
using System.IO;

namespace RoconBronzeOrders.Services.Implementation
{
    public class BronzeOrderService : IBronzeOrderService
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public BronzeOrderService(IConfigurationRoot config, IMapper mapper)
        {
            _config = config;
            _mapper = mapper;
        }

        public void processStart(OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var orderStatusConfigs = getOrderStatusConfigs(connection);
            Log.Debug("==> Number of status configurations: " + orderStatusConfigs.Count);
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = "SELECT SORORDERNUMBER AS CO_ODNO  FROM lcbo.LCO_ORDERS" 
                                    + " WHERE ordersource='O'"
                                    + " AND \"TIMESTAMP\" < TO_DATE('2019-11-03','yyyy-mm-dd')";

            var oraclReader = oraclCmd.ExecuteReader();

            while (oraclReader.Read())
            {
                try
                {
                    var orderStatusEntity = getNextOrderStatusEntity(oraclReader);
                    Log.Debug("OrderStatus ==> " + orderStatusEntity.OrderNumber);

                    var publishOrderStatuses = getOrderPublishStatus(orderStatusEntity, connection);

                    Order order = getRoconOrder(orderStatusEntity, orderStatusConfigs, publishOrderStatuses, connection);

                    var payload = JsonConvert.SerializeObject(order);

                    sendToFile(payload, order);
                }
                catch (Exception e)
                {
                    var errorInfo = $"A general error exception happened. Message = [{e.Message}. Stacktrace is: {e.StackTrace}]";
                    Log.Error(errorInfo);
                }
            }
            oraclReader.Close();
        }

        private void sendToFile(string payload, Order order)
        {
            // Log.Information($"Payload is: \n {payload} \n");
            var jsonFileOutput = _config["jsonFileOutput"];
            // Log.Information($"jsonFileOutput is: {jsonFileOutput}");

            var filename = jsonFileOutput.Replace("replace", order.orderHeader.sorOrderNumber.ToString());
            Log.Information($"Filename is: {filename}");

            using (StreamWriter w = File.AppendText(filename))
            {
                w.WriteLine(payload);
            }
        }

        private List<OrderStatusDetail> getOrderStatusConfigs(OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            //Log.Debug("Inside getOrderStatusConfig");
            List<OrderStatusDetail> orderStatusConfigs = new List<OrderStatusDetail>();
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = "SELECT STATUSCYCLEPATH, STATUSSEQUENCE, STATUSIDENTIFIER, STATUSDESC, CYCLEDESC FROM LCO_ORDER_STATUS_CONFIG";
            var oraclReader = oraclCmd.ExecuteReader();
            while (oraclReader.Read())
            {
                orderStatusConfigs.Add(new OrderStatusDetail
                {
                    statusPathName = ((string)oraclReader["CYCLEDESC"]).Trim(),
                    statusSequenceNo = Int32.Parse((string)oraclReader["STATUSSEQUENCE"]),
                    statusIdentifier = ((string)oraclReader["STATUSIDENTIFIER"]).Trim(),
                });
            }
            oraclReader.Close();
            return orderStatusConfigs;
        }

        private OrderStatusEntity getNextOrderStatusEntity(OracleDataReader oraclReader)
        {
            var orderStatusEntity = new OrderStatusEntity
            {
                OrderNumber = ((int) oraclReader["CO_ODNO"]).ToString()
            };
            return orderStatusEntity;
        }

        private List<OrderStatusDetail> getOrderPublishStatus(OrderStatusEntity orderStatusEntity, OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var oraclCmd = new OracleCommand("GET_ORDER_PUBLISHED_STATUS", connection);
            oraclCmd.CommandType = CommandType.StoredProcedure;

            oraclCmd.Parameters.Add("LV_SORORDERNUMBER", OracleDbType.Varchar2).Value = orderStatusEntity.OrderNumber;
            oraclCmd.Parameters.Add("REF_ORDERPUBLISHEDSTATUS", OracleDbType.RefCursor, ParameterDirection.InputOutput);

            var oraclReader = oraclCmd.ExecuteReader();

            List<OrderStatusDetail> publishOrderStatuses = new List<OrderStatusDetail>();
            while (oraclReader.Read())
            {
                publishOrderStatuses.Add(new OrderStatusDetail
                {
                    statusSequenceNo = (Int32.Parse(((string)oraclReader["statusSequenceNo"]).Trim())),
                    statusIdentifier = ((string)oraclReader["statusIdentifier"]).Trim(),
                    statusCreateTime = FormatUtils.AddTimezone(((DateTime)oraclReader["statusCreateTime"]).ToString())
                });
            }
            oraclReader.Close();
            return publishOrderStatuses;
        }

        private Order getRoconOrder(OrderStatusEntity orderStatusEntity,
                                           List<OrderStatusDetail> orderStatusConfigs,
                                           List<OrderStatusDetail> publishOrderStatuses,
                                           OracleConnection connection)
        {
            Task<Order> taskOrder = SQLHelper.GetOrderByIdAsync(orderStatusEntity,
                                                                 connection,
                                                                 _mapper);
            var order = taskOrder.Result;
            string systemIdentifier = "R";
            string systemName = "R:ROCON";
            order.orderHeader.orderSourceInfo = new OrderSourceInfo();
            order.orderHeader.orderSourceInfo.systemIdentifier = systemIdentifier;
            order.orderHeader.orderSourceInfo.systemName = systemName;
            order.orderHeader.orderSourceInfo.systemOrderStatusDetail = orderStatusConfigs.ToArray();
            order.orderHeader.publishedOrderStatus = removePublishedOrderStatus(order, publishOrderStatuses).ToArray();
            order.orderHeader.lcoOrderSystem = new SystemIdentify();
            order.orderHeader.lcoOrderSystem.systemIdentifier = systemIdentifier;
            order.orderHeader.lcoOrderSystem.systemName = systemName;
            order.orderHeader.sorSystem = new SystemIdentify();
            order.orderHeader.sorSystem.systemIdentifier = systemIdentifier;
            order.orderHeader.sorSystem.systemName = systemName;

            return order;
        }

        private List<OrderStatusDetail> removePublishedOrderStatus(Order order,
                                                                    List<OrderStatusDetail> publishOrderStatuses)
        {
            if (order.orderHeader.orderStatus != null)
            {
                var statusCode = order.orderHeader.orderStatus.orderStatusCode;
                var orderStatusDetails = new List<OrderStatusDetail>();
                foreach (OrderStatusDetail orderStatusDetail in publishOrderStatuses)
                {
                    orderStatusDetails.Add(orderStatusDetail);
                    // Log.Information($"=====> Status code: [{statusCode}] and status identifier [{orderStatusDetail.statusIdentifier}]]");
                    if (statusCode.Equals(orderStatusDetail.statusIdentifier))
                    {
                        // Log.Information("==========> Breaking");
                        break;
                    }
                }
                return orderStatusDetails;
            }
            return publishOrderStatuses;
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoconBronzeOrders.Services
{
    public interface IBronzeOrderService
    {
        void processStart(OracleConnection connection);
    }
}

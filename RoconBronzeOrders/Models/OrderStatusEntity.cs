﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoconBronzeOrders.Models
{
    public class OrderStatusEntity
    {
        public string OrderNumber { get; set; }
        public string Status { get; set; }
    }
}
